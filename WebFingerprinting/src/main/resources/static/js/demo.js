console.log("inside demo.js");

let button_fetchData  = document.getElementById("button_fetchData");
button_fetchData.addEventListener("click", buttonClickHandler)

function buttonClickHandler() {
    console.log("button working");

    let strOs = fetchOs();
    console.log("strOs = " + strOs);

    let strPlatform = fetchPlatform();
    console.log("strPlatform = " + strPlatform);

    let strDisplay = fetchDisplay();
    console.log("strDisplay = " + strDisplay);

    let strTimezone = fetchTimezone();
    console.log("strTimezone = " + strTimezone);
    
    let strUserAgent = fetchUserAgent();
    console.log("strUserAgent = " + strUserAgent);

    let strBrowser  = fetchBrowser();
    console.log("strBrowser = " + strBrowser);

    let strCanvas  = fetchCanvas();
    let strCanvasHash = hashMurmur3(strCanvas, 0);
    console.log("strCanvasHash = " + strCanvasHash);
    //console.log("strCanvas = " + strCanvas);

    let strLanguage  = fetchLanguage();
    console.log("strLanguage = " + strLanguage);

    let strFlash  = fetchFlash();
    console.log("strFlash = " + strFlash);

    let strConnection = fetchConnection();
    console.log("strConnection = " + strConnection);

    let strCookie = fetchCookie(); 
    console.log("strCookie = " + strCookie);

    let strJava = fetchJava();
    console.log("strJava = " + strJava);

    let strPlugins = fetchPlugins();
    console.log("strPlugins = " + strPlugins);
    
    let strFonts = fetchFonts();
    let strFontsHash = hashMurmur3(strFonts, 0);
    console.log("strFontsHash = " + strFontsHash);
    //console.log("strFonts = " + strFonts);

    let strWebGLVendor = fetchWebGLVendor();
    console.log("strWebGLVendor = " + strWebGLVendor);

    let strWebGLRendrer = fetchWebGLRendrer();
    console.log("strWebGLRendrer = " + strWebGLRendrer);
    
    const toSend = {
    		os: strOs ,
    		platform: strPlatform ,
    		display: strDisplay ,
    		timezone: strTimezone ,
    		userAgent: strUserAgent ,
    		browser: strBrowser ,
    		canvasHash: strCanvasHash ,
    		language: strLanguage ,
    		flash: strFlash ,
    		connection: strConnection ,
    		cookie: strCookie ,
    		java: strJava ,
    		plugins: strPlugins,
    		fontsHash: strFontsHash,
    		webglVendor: strWebGLVendor,
    		webglRendrer: strWebGLRendrer
    };
    
    jsonString = JSON.stringify(toSend);
    console.log(jsonString);
    
    let xhr = new XMLHttpRequest();
    xhr.open("POST", "/data");
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.send(jsonString);
    
    console.log(xhr);
    
    xhr.onreadystatechange = function() {
		if (xhr.readyState === 4) {
			document.getElementById("completefp").textContent = xhr.response;
		}
	}
    
    document.getElementById("os").textContent = strOs;
    document.getElementById("platform").textContent = strPlatform;
    document.getElementById("display").textContent = strDisplay;
    document.getElementById("timezone").textContent = strTimezone;
    document.getElementById("useragent").textContent = strUserAgent;
    document.getElementById("browser").textContent = strBrowser;
    document.getElementById("canvashash").textContent = strCanvasHash;
    document.getElementById("language").textContent = strLanguage;
    document.getElementById("flash").textContent = strFlash;
    document.getElementById("connection").textContent = strConnection;
    document.getElementById("cookie").textContent = strCookie;
    document.getElementById("java").textContent = strJava;
    document.getElementById("plugins").textContent = strPlugins;
    document.getElementById("fontshash").textContent = strFontsHash;
    document.getElementById("webglvendor").textContent = strWebGLVendor;
    document.getElementById("webglrendrer").textContent = strWebGLRendrer;
    
}

function fetchWebGLRendrer() {
    let canvas, gl, debugInfo, renderer, strError;

    strError = "Error";
    
    try {
        canvas = document.createElement("canvas");
        gl = canvas.getContext('webgl');
        debugInfo = gl.getExtension('WEBGL_debug_renderer_info');
        renderer = gl.getParameter(debugInfo.UNMASKED_RENDERER_WEBGL);
        return renderer;
    }
    catch (err) {
        return strError;
    }
}

function fetchWebGLVendor() {
    let canvas, gl, debugInfo, vendor, strError;

    strError = "Error";
    
    try {
        canvas = document.createElement("canvas");
        gl = canvas.getContext('webgl');
        debugInfo = gl.getExtension('WEBGL_debug_renderer_info');
         vendor = gl.getParameter(debugInfo.UNMASKED_VENDOR_WEBGL);
        return vendor;
    }
    catch (err) {
        return strError;
    }
}

function fetchFonts() {
    let strError, style, fonts, count, template, fragment, divs, i, font, div, body, result, e;

    strError = "Error";
    style = null;
    fonts = null;
    font = null;
    count = 0;
    template = null;
    divs = null;
    e = null;
    div = null;
    body = null;
    i = 0;

    try {
        style = "position: absolute; visibility: hidden; display: block !important";
        fonts = ["Abadi MT Condensed Light", "Adobe Fangsong Std", "Adobe Hebrew", "Adobe Ming Std", "Agency FB", "Aharoni", "Andalus", "Angsana New", "AngsanaUPC", "Aparajita", "Arab", "Arabic Transparent", "Arabic Typesetting", "Arial Baltic", "Arial Black", "Arial CE", "Arial CYR", "Arial Greek", "Arial TUR", "Arial", "Batang", "BatangChe", "Bauhaus 93", "Bell MT", "Bitstream Vera Serif", "Bodoni MT", "Bookman Old Style", "Braggadocio", "Broadway", "Browallia New", "BrowalliaUPC", "Calibri Light", "Calibri", "Californian FB", "Cambria Math", "Cambria", "Candara", "Castellar", "Casual", "Centaur", "Century Gothic", "Chalkduster", "Colonna MT", "Comic Sans MS", "Consolas", "Constantia", "Copperplate Gothic Light", "Corbel", "Cordia New", "CordiaUPC", "Courier New Baltic", "Courier New CE", "Courier New CYR", "Courier New Greek", "Courier New TUR", "Courier New", "DFKai-SB", "DaunPenh", "David", "DejaVu LGC Sans Mono", "Desdemona", "DilleniaUPC", "DokChampa", "Dotum", "DotumChe", "Ebrima", "Engravers MT", "Eras Bold ITC", "Estrangelo Edessa", "EucrosiaUPC", "Euphemia", "Eurostile", "FangSong", "Forte", "FrankRuehl", "Franklin Gothic Heavy", "Franklin Gothic Medium", "FreesiaUPC", "French Script MT", "Gabriola", "Gautami", "Georgia", "Gigi", "Gisha", "Goudy Old Style", "Gulim", "GulimChe", "GungSeo", "Gungsuh", "GungsuhChe", "Haettenschweiler", "Harrington", "Hei S", "HeiT", "Heisei Kaku Gothic", "Hiragino Sans GB", "Impact", "Informal Roman", "IrisUPC", "Iskoola Pota", "JasmineUPC", "KacstOne", "KaiTi", "Kalinga", "Kartika", "Khmer UI", "Kino MT", "KodchiangUPC", "Kokila", "Kozuka Gothic Pr6N", "Lao UI", "Latha", "Leelawadee", "Levenim MT", "LilyUPC", "Lohit Gujarati", "Loma", "Lucida Bright", "Lucida Console", "Lucida Fax", "Lucida Sans Unicode", "MS Gothic", "MS Mincho", "MS PGothic", "MS PMincho", "MS Reference Sans Serif", "MS UI Gothic", "MV Boli", "Magneto", "Malgun Gothic", "Mangal", "Marlett", "Matura MT Script Capitals", "Meiryo UI", "Meiryo", "Menlo", "Microsoft Himalaya", "Microsoft JhengHei", "Microsoft New Tai Lue", "Microsoft PhagsPa", "Microsoft Sans Serif", "Microsoft Tai Le", "Microsoft Uighur", "Microsoft YaHei", "Microsoft Yi Baiti", "MingLiU", "MingLiU-ExtB", "MingLiU_HKSCS", "MingLiU_HKSCS-ExtB", "Miriam Fixed", "Miriam", "Mongolian Baiti", "MoolBoran", "NSimSun", "Narkisim", "News Gothic MT", "Niagara Solid", "Nyala", "PMingLiU", "PMingLiU-ExtB", "Palace Script MT", "Palatino Linotype", "Papyrus", "Perpetua", "Plantagenet Cherokee", "Playbill", "Prelude Bold", "Prelude Condensed Bold", "Prelude Condensed Medium", "Prelude Medium", "PreludeCompressedWGL Black", "PreludeCompressedWGL Bold", "PreludeCompressedWGL Light", "PreludeCompressedWGL Medium", "PreludeCondensedWGL Black", "PreludeCondensedWGL Bold", "PreludeCondensedWGL Light", "PreludeCondensedWGL Medium", "PreludeWGL Black", "PreludeWGL Bold", "PreludeWGL Light", "PreludeWGL Medium", "Raavi", "Rachana", "Rockwell", "Rod", "Sakkal Majalla", "Sawasdee", "Script MT Bold", "Segoe Print", "Segoe Script", "Segoe UI Light", "Segoe UI Semibold", "Segoe UI Symbol", "Segoe UI", "Shonar Bangla", "Showcard Gothic", "Shruti", "SimHei", "SimSun", "SimSun-ExtB", "Simplified Arabic Fixed", "Simplified Arabic", "Snap ITC", "Sylfaen", "Symbol", "Tahoma", "Times New Roman Baltic", "Times New Roman CE", "Times New Roman CYR", "Times New Roman Greek", "Times New Roman TUR", "Times New Roman", "TlwgMono", "Traditional Arabic", "Trebuchet MS", "Tunga", "Tw Cen MT Condensed Extra Bold", "Ubuntu", "Umpush", "Univers", "Utopia", "Utsaah", "Vani", "Verdana", "Vijaya", "Vladimir Script", "Vrinda", "Webdings", "Wide Latin", "Wingdings"];
        count = fonts.length;
        template = '<b style="display:inline !important; width:auto !important; font:normal 10px/1 \'X\',sans-serif !important">ww</b>' + '<b style="display:inline !important; width:auto !important; font:normal 10px/1 \'X\',monospace !important">ww</b>';
        fragment = document.createDocumentFragment();
        divs = [];
        for (i = 0; i < count; i = i + 1) {
            font = fonts[i];
            div = document.createElement('div');
            font = font.replace(/['"<>]/g, '');
            div.innerHTML = template.replace(/X/g, font);
            div.style.cssText = style;
            fragment.appendChild(div);
            divs.push(div);
        }
        body = document.body;
        body.insertBefore(fragment, body.firstChild);
        result = [];
        for (i = 0; i < count; i = i + 1) {
            e = divs[i].getElementsByTagName('b');
            if (e[0].offsetWidth === e[1].offsetWidth) {
                result.push(fonts[i]);
            }
        }
        for (i = 0; i < count; i = i + 1) {
            body.removeChild(divs[i]);
        }
        return result.join('|');
    } 
    catch (err) {
        return strError;
    }
}

function fetchPlugins() {
    let strPlugins, strError;

    strPlugins = "N/A";
    strError = "Error";
    
    try {
        bolFirst = true;

        if (navigator.plugins.length > 0) {
            for (i = 0; i < navigator.plugins.length; i++) {
                if (bolFirst === true) {
                	strPlugins = "";
                    strPlugins += navigator.plugins[i].name;
                    bolFirst = false;
                } else {
                    strPlugins += "|" + navigator.plugins[i].name;
                }
            }
        }
        
        return strPlugins;
    }
    catch (err) {
        return strError;
    }
}

function fetchLanguage() {
    let strError, strLanguage;

    strError = "Error";

    try {
        strLanguage = navigator.language;
        return strLanguage;
    } 
    catch (err) {
        return strError;
    }
}

function fetchConnection() {
    let strError, strConnection;

    strError = "Error";
    strConnection = null;

    try {
        strConnection = navigator.connection.type;
        return strConnection;
    } 
    catch (err) {
        return strError;
    }
}

function fetchCookie() {
    let strError, bolCookieEnabled, bolOut;

    strError = "Error";
    bolCookieEnabled = null;
    bolOut = null;

    try {
        bolCookieEnabled = (navigator.cookieEnabled) ? true : false;
        if (typeof navigator.cookieEnabled === "undefined" && !bolCookieEnabled) {
            document.cookie = "testcookie";
            bolCookieEnabled = (document.cookie.indexOf("testcookie") !== -1) ? true : false;
        }
        bolOut = bolCookieEnabled;
        return bolOut;
    } 
    catch (err) {
        return strError;
    }
}

function fetchJava() {
    let strError, strJavaEnabled, strOut;

    strError = "Error";
    strJavaEnabled = null;
    strOut = null;

    try {
        if (navigator.javaEnabled()) {
            strJavaEnabled = "true";
        } else {
            strJavaEnabled = "false";
        }
        strOut = strJavaEnabled;
        return strOut;
    } 
    catch (err) {
        return strError;
    }
}

function fetchUserAgent() {
    let strSep, strTmp, strUserAgent, strOut;

    strSep = "|";
    strError = "Error";
    strTmp = null;
    strUserAgent = null;
    strOut = null;

    try {
        strUserAgent = navigator.userAgent.toLowerCase();
        strTmp = strUserAgent + strSep + navigator.platform;
        if (navigator.cpuClass) {
            strTmp += strSep + navigator.cpuClass;
        }
        if (navigator.browserLanguage) {
            strTmp += strSep + navigator.browserLanguage;
        } 
        else {
            strTmp += strSep + navigator.language;
        }
        strOut = strTmp;
        return strOut;
    }
    catch (err) {
        return strError;
    }
    
}

function fetchTimezone() {
    let strError, dtDate, numOffset, numGMTHours, numOut;

    strError = "Error";
    dtDate = null;
    numOffset = null;
    numGMTHours = null;
    numOut = null;

    try {
        dtDate = new Date();
        numOffset = dtDate.getTimezoneOffset();
        numGMTHours = (numOffset / 60) * (-1);
        numOut = numGMTHours;
        return numOut;
    } 
    catch (err) {
        return strError;
    }
}

function fetchPlatform() {
    let strPlatform, strError;

    strPlatform = null;
    strError = "Error";

    try {
        strPlatform = navigator.platform.toLocaleLowerCase();
        return strPlatform;
    }
    catch (err) {
        return strError;
    }
}

function fetchOs() {
    let strSep, strError, strUserAgent, strOS, strOSBits, strOut;

    strSep = "|";
    strError = "Error";
    strUserAgent = null;
    strOS = null;
    strOSBits = null;
    strOut = null;

    try {
        strUserAgent = navigator.userAgent.toLowerCase();

        if (strUserAgent.indexOf("windows nt 6.3") !== -1) {
            strOS = "Windows 8.1";
        } else if (strUserAgent.indexOf("windows nt 6.2") !== -1) {
            strOS = "Windows 8";
        } else if (strUserAgent.indexOf("windows nt 6.1") !== -1) {
            strOS = "Windows 7";
        } else if (strUserAgent.indexOf("windows nt 10.0") !== -1) {
            strOS = "Windows 10";
        } else if (strUserAgent.indexOf("windows") !== -1) {
            strOS = "Windows (Unknown Version)";
        } else if (strUserAgent.indexOf("ubuntu") !== -1) {
            strOS = "Ubuntu";
        } else if (strUserAgent.indexOf("linux") !== -1) {
            strOS = "Linux";
        } else if (strUserAgent.indexOf("ipad") !== -1) {
            strOS = "iOS (iPad)";;
        } else if (strUserAgent.indexOf("iphone") !== -1) {
            strOS = "iOS (iPhone)";
        } else if (strUserAgent.indexOf("android") !== -1) {
            strOS = "Android";
        } else if (strUserAgent.indexOf("mac os x 10.10") !== -1) {
            strOS = "Mac OSX Yosemite";
        } else if (strUserAgent.indexOf("mac os x 10.11") !== -1) {
            strOS = "Mac OSX El Capitan";
        } else if (strUserAgent.indexOf("mac") !== -1) {
            strOS = "Mac OS (Version Unknown)";
        } else {
            strOS = "Unknown";
        }

        if (strUserAgent.indexOf("x64") !== -1) {
            strOSBits = "64 bits";
        } else if (strUserAgent.indexOf("x32") !== -1) {
            strOSBits = "32 bits";
        } else if (strUserAgent.indexOf("x86") !== -1) {
            strOSBits = "32 bits*";
        } else if (strUserAgent.indexOf("wow64") !== -1) {
            strOSBits = "64 bits";
        } else if (strUserAgent.indexOf("win64") !== -1) {
            strOSBits = "64 bits";
        } else if (strUserAgent.indexOf("win32") !== -1) {
            strOSBits = "32 bits";
        } else if (strUserAgent.indexOf("iphone") !== -1) {
            strOSBits = "32 bits";
        } else if (strUserAgent.indexOf("android") !== -1) {
            strOSBits = "32 bits";
        } else {
            strOSBits = "Unknown";
        }
        strOut = strOS + strSep + strOSBits;
        return strOut;
    } 
    catch (err) {
        return strError;
    }
}

function fetchDisplay() {
    let strSep, strPair, strError, strScreen, strDisplay, strOut;

    strSep = "|";
    strPair = "=";
    strError = "Error";
    strScreen = null;
    strDisplay = null;
    strOut = null;

    try {
        strScreen = window.screen;
        if (strScreen) {
            strDisplay = strScreen.colorDepth + strSep + strScreen.width + strSep + strScreen.height + strSep + strScreen.availWidth + strSep + strScreen.availHeight;
        }
        strOut = strDisplay;
        return strOut;
    } 
    catch (err) {
        return strError;
    }
}

function fetchCanvas() {
    let strError, canvas, strCText, strText, strOut;

    strError = "Error";
    canvas = null;
    strCText = null;
    strText = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ`~1!2@3#4$5%6^7&8*9(0)-_=+[{]}|;:',<.>/?";
    strOut = null;

    try {
        canvas = document.createElement('canvas');
        strCText = canvas.getContext('2d');
        strCText.textBaseline = "top";
        strCText.font = "14px 'Arial'";
        strCText.textBaseline = "alphabetic";
        strCText.fillStyle = "#f60";
        strCText.fillRect(125, 1, 62, 20);
        strCText.fillStyle = "#069";
        strCText.fillText(strText, 2, 15);
        strCText.fillStyle = "rgba(102, 204, 0, 0.7)";
        strCText.fillText(strText, 4, 17);
        strOut = canvas.toDataURL();
        return strOut;
    } 
    catch (err) {
        return strError;
    }
}


function fetchFlash() {
    let strError, objPlayerVersion, strVersion, strOut;

    strError = "N/A";
    objPlayerVersion = null;
    strVersion = null;
    strOut = null;

    try {
        objPlayerVersion = swfobject.getFlashPlayerVersion();
        strVersion = objPlayerVersion.major + "." + objPlayerVersion.minor + "." + objPlayerVersion.release;
        if (strVersion === "0.0.0") {
            strVersion = "N/A";
        }
        strOut = strVersion;
        return strOut;
    } 
    catch (err) {
        return strError;
    }
}


function fetchBrowser() {
    let strError, strUserAgent, numVersion, strBrowser, strOut;

    strError = "Error";
    strUserAgent = null;
    numVersion = null;
    strBrowser  = null;
    strOut = null;

    try {
        strUserAgent = navigator.userAgent.toLowerCase();
        if (/msie (\d+\.\d+);/.test(strUserAgent)) {
            numVersion = Number(RegExp.$1);
            strBrowser = "Internet Explorer " + numVersion;
        }  else if (/firefox[\/\s](\d+\.\d+)/.test(strUserAgent)) {
            numVersion = Number(RegExp.$1);
            strBrowser = "Firefox " + numVersion;
        } else if (/opera[\/\s](\d+\.\d+)/.test(strUserAgent)) {
            numVersion = Number(RegExp.$1);
            strBrowser = "Opera " + numVersion;
        } else if (/edge[\/\s](\d+\.\d+)/.test(strUserAgent)) {
            numVersion = Number(RegExp.$1);
            strBrowser = "Edge " + numVersion;
        } else if (/chrome[\/\s](\d+\.\d+)/.test(strUserAgent)) {
            numVersion = Number(RegExp.$1);
            strBrowser = "Chrome " + numVersion;
        } else if (/version[\/\s](\d+\.\d+)/.test(strUserAgent)) {
            numVersion = Number(RegExp.$1);
            strBrowser = "Safari " + numVersion;
        } else {
            strBrowser = "Unknown";
        }
        strOut = strBrowser;
        return strOut;
    } 
    catch (err) {
        return strError;
    }
}


function hashMurmur3(key, seed) {
	let remainder, bytes, h1, h1b, c1, c1b, c2, c2b, k1, i;
	
	remainder = key.length & 3;
	bytes = key.length - remainder;
	h1 = seed;
	c1 = 0xcc9e2d51;
	c2 = 0x1b873593;
	i = 0;
	
	while (i < bytes) {
	  	k1 = 
	  	  ((key.charCodeAt(i) & 0xff)) |
	  	  ((key.charCodeAt(++i) & 0xff) << 8) |
	  	  ((key.charCodeAt(++i) & 0xff) << 16) |
	  	  ((key.charCodeAt(++i) & 0xff) << 24);
		++i;
		
		k1 = ((((k1 & 0xffff) * c1) + ((((k1 >>> 16) * c1) & 0xffff) << 16))) & 0xffffffff;
		k1 = (k1 << 15) | (k1 >>> 17);
		k1 = ((((k1 & 0xffff) * c2) + ((((k1 >>> 16) * c2) & 0xffff) << 16))) & 0xffffffff;

		h1 ^= k1;
        h1 = (h1 << 13) | (h1 >>> 19);
		h1b = ((((h1 & 0xffff) * 5) + ((((h1 >>> 16) * 5) & 0xffff) << 16))) & 0xffffffff;
		h1 = (((h1b & 0xffff) + 0x6b64) + ((((h1b >>> 16) + 0xe654) & 0xffff) << 16));
	}
	
	k1 = 0;
	
	switch (remainder) {
		case 3: k1 ^= (key.charCodeAt(i + 2) & 0xff) << 16;
		case 2: k1 ^= (key.charCodeAt(i + 1) & 0xff) << 8;
		case 1: k1 ^= (key.charCodeAt(i) & 0xff);
		
		k1 = (((k1 & 0xffff) * c1) + ((((k1 >>> 16) * c1) & 0xffff) << 16)) & 0xffffffff;
		k1 = (k1 << 15) | (k1 >>> 17);
		k1 = (((k1 & 0xffff) * c2) + ((((k1 >>> 16) * c2) & 0xffff) << 16)) & 0xffffffff;
		h1 ^= k1;
	}
	
	h1 ^= key.length;

	h1 ^= h1 >>> 16;
	h1 = (((h1 & 0xffff) * 0x85ebca6b) + ((((h1 >>> 16) * 0x85ebca6b) & 0xffff) << 16)) & 0xffffffff;
	h1 ^= h1 >>> 13;
	h1 = ((((h1 & 0xffff) * 0xc2b2ae35) + ((((h1 >>> 16) * 0xc2b2ae35) & 0xffff) << 16))) & 0xffffffff;
	h1 ^= h1 >>> 16;

	return h1 >>> 0;
}