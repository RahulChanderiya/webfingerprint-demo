package com.RahulChanderiya.WebFingerprinting;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebFingerprintingApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebFingerprintingApplication.class, args);
	}

}
