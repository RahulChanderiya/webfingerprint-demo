package com.RahulChanderiya.WebFingerprinting.database;

import com.RahulChanderiya.WebFingerprinting.attributes.*;

import java.math.BigInteger; 
import java.security.MessageDigest; 
import java.security.NoSuchAlgorithmException; 

import java.sql.*;

public class database {

	private Connection con;
	
	public database() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver"); 
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/webfingerprint?autoReconnect=true&useSSL=false", "root", "root");
		}
		catch(Exception exc) {
			System.out.println(exc);
		}
	}
	
	private String getMd5(String input) 
    { 
        try { 
            MessageDigest md = MessageDigest.getInstance("MD5"); 
            byte[] messageDigest = md.digest(input.getBytes()); 
            BigInteger no = new BigInteger(1, messageDigest); 
  
            String hashtext = no.toString(16); 
            while (hashtext.length() < 32) { 
                hashtext = "0" + hashtext; 
            } 
            return hashtext; 
        }  
        catch (NoSuchAlgorithmException e) { 
            throw new RuntimeException(e); 
        } 
    } 
	
	private boolean checkCompleteFp(String complete_fp) {
		try {
			String querry = "select complete_fp from web_attributes where complete_fp = ?";
			PreparedStatement stmt = con.prepareStatement(querry);
			stmt.setString(1, complete_fp);
			ResultSet rs = stmt.executeQuery();
			
			if(rs.next()) {
				System.out.println("complete_fp already present in DB");
				return true;
			}
			else {
				System.out.println("new complete_fp found");
				return false;
			}
		}
		catch(Exception exc) {
			System.out.println(exc);
			return false;
		}
	}
	
	public String getCompleteFp(WebAttributes attributes) {
		String complete_fp = getMd5(attributes.toString());
		
		if(checkCompleteFp(complete_fp) == true) {
			return complete_fp;
		}
		
		try {
			String querry = "insert into web_attributes value (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement stmt = con.prepareStatement(querry);
			stmt.setString(1, complete_fp);
			stmt.setString(2, attributes.getOs());
			stmt.setString(3, attributes.getPlatform());
			stmt.setString(4, attributes.getDisplay());
			stmt.setString(5, attributes.getTimezone());
			stmt.setString(6, attributes.getUserAgent());
			stmt.setString(7, attributes.getBrowser());
			stmt.setString(8, attributes.getCanvasHash());
			stmt.setString(9, attributes.getLanguage());
			stmt.setString(10, attributes.getFlash());
			stmt.setString(11, attributes.getConnection());
			stmt.setString(12, attributes.getCookie());
			stmt.setString(13, attributes.getJava());
			stmt.setString(14, attributes.getPlugins());
			stmt.setString(15, attributes.getFontsHash());
			stmt.setString(16, attributes.getWebglVendor());
			stmt.setString(17, attributes.getWebglRendrer());
			
			stmt.execute();
			stmt.close();
			System.out.println("Data Inserted");
			return complete_fp;
		}
		
		catch(Exception exc) {
			System.out.println(exc);
			return "Error";
		}

	}
	
}


/*create table web_attributes(
	complete_fp varchar(225),
	os varchar(255),
	platform varchar(255),
	display varchar(255),
	timezone varchar(255),
	userAgent varchar(255),
	browser varchar(255),
	canvasHash varchar(255),
	languagex varchar(255),
	flash varchar(255),
	connectionx varchar(255),
	cookie varchar(255),
	java varchar(255),
	pluginsx varchar(255),
	fontsHash varchar(255),
	webglVendor varchar(255),
	webglRendrer varchar(255)
);*/

