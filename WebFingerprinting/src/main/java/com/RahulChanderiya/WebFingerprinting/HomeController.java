package com.RahulChanderiya.WebFingerprinting;
import com.RahulChanderiya.WebFingerprinting.attributes.*;

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.boot.jackson.JsonObjectDeserializer;
import org.springframework.boot.jackson.JsonObjectSerializer;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonParser;

import com.RahulChanderiya.WebFingerprinting.database.*;

@RestController
public class HomeController {
	
	@RequestMapping("/home")
	public ModelAndView getHomePage() {
	    ModelAndView modelAndView = new ModelAndView();
	    modelAndView.setViewName("demo");
	    System.out.println("inside getHomePage()");
	    return modelAndView;
	}
	
	@PostMapping(path = "/data", consumes = "application/json", produces = "application/json")
	public @ResponseBody String fetchFingerprint(@RequestBody WebAttributes attirubutes) {
		System.out.println("inside fetchFingerprint()");
		System.out.println(attirubutes);
		
		database webFingerprintLib = new database();
		String complete_fp = webFingerprintLib.getCompleteFp(attirubutes);
		
		System.out.println("complete_fp = " + complete_fp);
		
		return complete_fp;
	}
	
}
