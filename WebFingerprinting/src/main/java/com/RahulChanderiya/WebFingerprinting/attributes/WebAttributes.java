package com.RahulChanderiya.WebFingerprinting.attributes;

public class WebAttributes {
	private String os;
	private String platform;
	private String display;
	private String timezone;
	private String userAgent;
	private String browser;
	private String canvasHash;
	private String language;
	private String flash;
	private String connection;
	private String cookie;
	private String java;
	private String plugins;
	private String fontsHash;
	private String webglVendor;
	private String webglRendrer;
	
	
	public WebAttributes() {
			
	}
	
	public WebAttributes(String os, String platform, String display, String timezone, String userAgent, String browser,
			String canvasHash, String language, String flash, String connection, String cookie, String java,
			String plugins, String fontsHash, String webglVendor, String webglRendrer) {
		super();
		this.os = os;
		this.platform = platform;
		this.display = display;
		this.timezone = timezone;
		this.userAgent = userAgent;
		this.browser = browser;
		this.canvasHash = canvasHash;
		this.language = language;
		this.flash = flash;
		this.connection = connection;
		this.cookie = cookie;
		this.java = java;
		this.plugins = plugins;
		this.fontsHash = fontsHash;
		this.webglVendor = webglVendor;
		this.webglRendrer = webglRendrer;
	}
	
	
	@Override
	public String toString() {
		return "WebAttributes [os=" + os + ", platform=" + platform + ", display=" + display + ", timezone=" + timezone
				+ ", userAgent=" + userAgent + ", browser=" + browser + ", canvasHash=" + canvasHash + ", language="
				+ language + ", flash=" + flash + ", connection=" + connection + ", cookie=" + cookie + ", java=" + java
				+ ", plugins=" + plugins + ", fontsHash=" + fontsHash + ", webglVendor=" + webglVendor + ", webglRendrer="
				+ webglRendrer + "]";
	}



	public String getOs() {
		return os;
	}
	public void setOs(String os) {
		this.os = os;
	}
	public String getPlatform() {
		return platform;
	}
	public void setPlatform(String platform) {
		this.platform = platform;
	}
	public String getDisplay() {
		return display;
	}
	public void setDisplay(String display) {
		this.display = display;
	}
	public String getTimezone() {
		return timezone;
	}
	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}
	public String getUserAgent() {
		return userAgent;
	}
	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}
	public String getBrowser() {
		return browser;
	}
	public void setBrowser(String browser) {
		this.browser = browser;
	}
	public String getCanvasHash() {
		return canvasHash;
	}
	public void setCanvasHash(String canvasHash) {
		this.canvasHash = canvasHash;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getFlash() {
		return flash;
	}
	public void setFlash(String flash) {
		this.flash = flash;
	}
	public String getConnection() {
		return connection;
	}
	public void setConnection(String connection) {
		this.connection = connection;
	}
	public String getCookie() {
		return cookie;
	}
	public void setCookie(String cookie) {
		this.cookie = cookie;
	}
	public String getJava() {
		return java;
	}
	public void setJava(String java) {
		this.java = java;
	}
	public String getPlugins() {
		return plugins;
	}
	public void setPlugins(String plugins) {
		this.plugins = plugins;
	}
	public String getFontsHash() {
		return fontsHash;
	}
	public void setFontsHash(String fontsHash ) {
		this.fontsHash = fontsHash ;
	}
	public String getWebglVendor() {
		return webglVendor;
	}
	public void setWebglVendor(String webglVendor) {
		this.webglVendor = webglVendor;
	}
	public String getWebglRendrer() {
		return webglRendrer;
	}
	public void setWebglRendrer(String webglRendrer) {
		this.webglRendrer = webglRendrer;
	}
	
	
}
